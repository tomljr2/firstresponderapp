package edu.fsu.cs.mobile.firstresponderapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class EditProfileActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);

        final EditText FNET = findViewById(R.id.FNET);
        final EditText LNET = findViewById(R.id.LNET);
        final EditText BDET = findViewById(R.id.calendar);
        final EditText PET = findViewById(R.id.pedET);
        final EditText ECET = findViewById(R.id.emergencyET);
        final EditText CDET = findViewById(R.id.CDET);
        final EditText MET = findViewById(R.id.MedET);
        final EditText AET = findViewById(R.id.AllET);
        final EditText SET = findViewById(R.id.SurET);

        final RadioButton MRB = findViewById(R.id.male);
        final RadioButton FRB = findViewById(R.id.female);

        final Spinner BTL = findViewById(R.id.letterSpinner);
        final Spinner BTPM = findViewById(R.id.PMSpinner);


        Button submit = findViewById(R.id.Submit);
        if(getIntent().getStringExtra("username").equals("child"))
        {
            final DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("Parent").
                    child(getIntent().getStringExtra("parentID")).child("Children").child(getIntent().getStringExtra("ID"));

            ref.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if(dataSnapshot.child("firstName").getValue() != null)
                        FNET.setText(dataSnapshot.child("firstName").getValue().toString());
                    if(dataSnapshot.child("lastName").getValue() != null)
                        LNET.setText(dataSnapshot.child("lastName").getValue().toString());
                    if(dataSnapshot.child("birthDate").getValue() != null)
                        BDET.setText(dataSnapshot.child("birthDate").getValue().toString());
                    if(dataSnapshot.child("gender").getValue() != null)
                    {
                        if(dataSnapshot.child("gender").getValue().equals("Male"))
                            MRB.setChecked(true);
                        else
                            FRB.setChecked(true);
                    }
                    if(dataSnapshot.child("pediatricianContact").getValue() != null)
                        PET.setText(dataSnapshot.child("pediatricianContact").getValue().toString());
                    if(dataSnapshot.child("emergencyContact").getValue() != null)
                        ECET.setText(dataSnapshot.child("emergencyContact").getValue().toString());
                    if(dataSnapshot.child("cognitiveDisabilities").getValue() != null)
                        CDET.setText(dataSnapshot.child("cognitiveDisabilities").getValue().toString());
                    if(dataSnapshot.child("medications").getValue() != null)
                        MET.setText(dataSnapshot.child("medications").getValue().toString());
                    if(dataSnapshot.child("allergies").getValue() != null)
                        AET.setText(dataSnapshot.child("allergies").getValue().toString());
                    if(dataSnapshot.child("surgeries").getValue() != null)
                        SET.setText(dataSnapshot.child("surgeries").getValue().toString());

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {}
            });

            submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ref.child("firstName").setValue(FNET.getText().toString());
                    ref.child("lastName").setValue(LNET.getText().toString());
                    ref.child("birthDate").setValue(BDET.getText().toString());
                    if(MRB.isChecked())
                        ref.child("gender").setValue("Male");
                    else
                        ref.child("gender").setValue("Female");
                    ref.child("pediatricianContact").setValue(PET.getText().toString());
                    ref.child("emergencyContact").setValue(ECET.getText().toString());
                    ref.child("cognitiveDisabilities").setValue(CDET.getText().toString());
                    ref.child("bloodTypeLetter").setValue(BTL.getSelectedItem().toString());
                    ref.child("bloodTypePM").setValue(BTPM.getSelectedItem().toString());
                    ref.child("medications").setValue(MET.getText().toString());
                    ref.child("allergies").setValue(AET.getText().toString());
                    ref.child("surgeries").setValue(SET.getText().toString());

                    if(getIntent().getStringExtra("username").equals("child"))
                    {
                        Intent i = new Intent(getApplicationContext(), ParentAccountActivity.class);
                        i.putExtra("username", getIntent().getStringExtra("parentUsername"));
                        startActivity(i);
                    }
                    else
                    {
                        Intent i = new Intent(getApplicationContext(), ParentAccountActivity.class);
                        i.putExtra("username", getIntent().getStringExtra("username"));
                        startActivity(i);
                    }
                }
            });
        }
        else {
            final DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("Parent").
                    child(getIntent().getStringExtra("ID"));

            ref.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if(dataSnapshot.child("firstName").getValue() != null)
                        FNET.setText(dataSnapshot.child("firstName").getValue().toString());
                    if(dataSnapshot.child("lastName").getValue() != null)
                        LNET.setText(dataSnapshot.child("lastName").getValue().toString());
                    if(dataSnapshot.child("birthDate").getValue() != null)
                        BDET.setText(dataSnapshot.child("birthDate").getValue().toString());
                    if(dataSnapshot.child("gender").getValue() != null)
                    {
                        if(dataSnapshot.child("gender").getValue().equals("Male"))
                            MRB.setChecked(true);
                        else
                            FRB.setChecked(true);
                    }
                    if(dataSnapshot.child("pediatricianContact").getValue() != null)
                        PET.setText(dataSnapshot.child("pediatricianContact").getValue().toString());
                    if(dataSnapshot.child("emergencyContact").getValue() != null)
                        ECET.setText(dataSnapshot.child("emergencyContact").getValue().toString());
                    if(dataSnapshot.child("cognitiveDisabilities").getValue() != null)
                        CDET.setText(dataSnapshot.child("cognitiveDisabilities").getValue().toString());
                    if(dataSnapshot.child("medications").getValue() != null)
                        MET.setText(dataSnapshot.child("medications").getValue().toString());
                    if(dataSnapshot.child("allergies").getValue() != null)
                        AET.setText(dataSnapshot.child("allergies").getValue().toString());
                    if(dataSnapshot.child("surgeries").getValue() != null)
                        SET.setText(dataSnapshot.child("surgeries").getValue().toString());

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {}
            });

            submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ref.child("firstName").setValue(FNET.getText().toString());
                    ref.child("lastName").setValue(LNET.getText().toString());
                    ref.child("birthDate").setValue(BDET.getText().toString());
                    if(MRB.isChecked())
                        ref.child("gender").setValue("Male");
                    else
                        ref.child("gender").setValue("Female");
                    ref.child("pediatricianContact").setValue(PET.getText().toString());
                    ref.child("emergencyContact").setValue(ECET.getText().toString());
                    ref.child("cognitiveDisabilities").setValue(CDET.getText().toString());
                    ref.child("bloodTypeLetter").setValue(BTL.getSelectedItem().toString());
                    ref.child("bloodTypePM").setValue(BTPM.getSelectedItem().toString());
                    ref.child("medications").setValue(MET.getText().toString());
                    ref.child("allergies").setValue(AET.getText().toString());
                    ref.child("surgeries").setValue(SET.getText().toString());

                    if(getIntent().getStringExtra("username").equals("child"))
                    {
                        Intent i = new Intent(getApplicationContext(), ParentAccountActivity.class);
                        i.putExtra("username", getIntent().getStringExtra("parentUsername"));
                        startActivity(i);
                    }
                    else
                    {
                        Intent i = new Intent(getApplicationContext(), ParentAccountActivity.class);
                        i.putExtra("username", getIntent().getStringExtra("username"));
                        startActivity(i);
                    }
                }
            });
        }


    }
}
