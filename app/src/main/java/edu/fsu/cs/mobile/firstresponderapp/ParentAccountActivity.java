package edu.fsu.cs.mobile.firstresponderapp;

import android.content.Intent;
import android.content.SearchRecentSuggestionsProvider;
import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;

import java.util.ArrayList;

public class ParentAccountActivity extends AppCompatActivity {

    final StringBuilder ID = new StringBuilder();   //This ID will be used for quicker access to the
                                                    //the child in the future.

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parent_account);

        final TextView parentName = findViewById(R.id.parentName);
        final TextView parentBD = findViewById(R.id.parentBD);
        final TextView parentGender = findViewById(R.id.parentGender);
        final TextView emergencyContact = findViewById(R.id.emergencyContact2);
        final TextView pedContact = findViewById(R.id.pedContact2);
        final TextView BloodType = findViewById(R.id.BloodType2);
        final TextView meds = findViewById(R.id.meds2);
        final TextView allergies = findViewById(R.id.allergies2);
        final TextView disabilities = findViewById(R.id.disabilities2);
        final TextView surgeries = findViewById(R.id.surgeries2);
        final ImageView qr = findViewById(R.id.image);

        final ListView childrenList = findViewById(R.id.childrenList);
        final ArrayList<String> children = new ArrayList<String>();

        final DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("Parent");

        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    if(snapshot.child("username").getValue().toString().equals(getIntent().getStringExtra("username")))
                    {
                        ID.append(snapshot.getKey());
                        parentName.setText(snapshot.child("firstName").getValue().toString()
                            + " " + snapshot.child("lastName").getValue().toString());
                        if(snapshot.child("birthDate").getValue() != null)
                            parentBD.setText(snapshot.child("birthDate").getValue().toString());
                        if(snapshot.child("gender").getValue() != null)
                            parentGender.setText(snapshot.child("gender").getValue().toString());
                        if(snapshot.child("emergencyContact").getValue() != null)
                            emergencyContact.setText(snapshot.child("emergencyContact").getValue().toString());
                        if(snapshot.child("pediatricianContact").getValue() != null)
                            pedContact.setText(snapshot.child("pediatricianContact").getValue().toString());
                        if((snapshot.child("bloodTypeLetter").getValue() != null) && (snapshot.child("bloodTypePM").getValue() != null))
                            BloodType.setText(snapshot.child("bloodTypeLetter").getValue().toString()
                            + snapshot.child("bloodTypePM").getValue().toString());
                        if(snapshot.child("medications").getValue() != null)
                            meds.setText(snapshot.child("medications").getValue().toString());
                        if(snapshot.child("allergies").getValue() != null)
                            allergies.setText(snapshot.child("allergies").getValue().toString());
                        if(snapshot.child("cognitiveDisabilities").getValue() != null)
                            disabilities.setText(snapshot.child("cognitiveDisabilities").getValue().toString());
                        if(snapshot.child("surgeries").getValue() != null)
                            surgeries.setText(snapshot.child("surgeries").getValue().toString());
                        if(snapshot.child("Children").getValue() != null)
                        {
                            for(DataSnapshot snapshot1: snapshot.child("Children").getChildren())
                            {
                                children.add(snapshot1.child("firstName").getValue().toString()
                                    + " " + snapshot1.child("lastName").getValue().toString());
                            }

                            ArrayAdapter<String> adapter =  new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_list_item_1, children);
                            childrenList.setAdapter(adapter);
                            adapter.notifyDataSetChanged();

                            childrenList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(final AdapterView<?> adapterView, View view, final int i, long l) {
                                    ref.child(ID.toString()).child("Children").addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(DataSnapshot dataSnapshot) {
                                            for(DataSnapshot snapshot1: dataSnapshot.getChildren())
                                            {
                                                if((snapshot1.child("firstName").getValue().toString() + " " +
                                                        snapshot1.child("lastName").getValue().toString()).
                                                        equals(childrenList.getItemAtPosition(i).toString()))
                                                {
                                                    StringBuilder childID = new StringBuilder();
                                                    Intent intent = new Intent(getApplicationContext(),ChildProfileActivity.class);

                                                    childID.append(snapshot1.getKey());
                                                    intent.putExtra("ID", ID.toString());
                                                    intent.putExtra("childID", childID.toString());

                                                    startActivity(intent);
                                                }
                                            }
                                        }

                                        @Override
                                        public void onCancelled(DatabaseError databaseError) {}
                                    });
                                }
                            });
                        }
                        try {
                            qr.setImageBitmap(textToImage(ID.toString(),500,500));
                        } catch (WriterException e) {
                            e.printStackTrace();
                        }

                        return;
                    }
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }

    private Bitmap textToImage(String text, int width, int height) throws WriterException, NullPointerException {
        BitMatrix bitMatrix;
        try {
            bitMatrix = new MultiFormatWriter().encode(text, BarcodeFormat.QR_CODE,
                    width, height, null);
        } catch (IllegalArgumentException Illegalargumentexception) {
            return null;
        }

        int bitMatrixWidth = bitMatrix.getWidth();
        int bitMatrixHeight = bitMatrix.getHeight();
        int[] pixels = new int[bitMatrixWidth * bitMatrixHeight];

        int colorWhite = 0xFFFFFFFF;
        int colorBlack = 0xFF000000;

        for (int y = 0; y < bitMatrixHeight; y++) {
            int offset = y * bitMatrixWidth;
            for (int x = 0; x < bitMatrixWidth; x++) {
                pixels[offset + x] = bitMatrix.get(x, y) ? colorBlack : colorWhite;
            }
        }
        Bitmap bitmap = Bitmap.createBitmap(bitMatrixWidth, bitMatrixHeight, Bitmap.Config.ARGB_4444);

        bitmap.setPixels(pixels, 0, width, 0, 0, bitMatrixWidth, bitMatrixHeight);
        return bitmap;
    }

    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.parentmenu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case R.id.editProfile:
                Intent intent = new Intent(this, EditProfileActivity.class);
                intent.putExtra("ID", ID.toString());
                intent.putExtra("username",getIntent().getStringExtra("username"));
                startActivity(intent);
                break;
            case R.id.addChildren:
                final DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("Parent").child(ID.toString()).child("Children");
                final StringBuilder childID = new StringBuilder();
                ref.push().setValue(new newUser("","","",""));

                ref.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        for(DataSnapshot snapshot : dataSnapshot.getChildren()) {
                            if(snapshot.child("firstName").getValue().equals(""))
                                childID.append(snapshot.getKey());
                        }

                        Intent intent2 = new Intent(getApplicationContext(), EditProfileActivity.class);
                        intent2.putExtra("ID", childID.toString());
                        intent2.putExtra("username", "child");
                        intent2.putExtra("parentID", ID.toString());
                        intent2.putExtra("parentUsername", getIntent().getStringExtra("username"));
                        startActivity(intent2);
                    }
                    @Override
                    public void onCancelled(DatabaseError databaseError) {}
                });
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
