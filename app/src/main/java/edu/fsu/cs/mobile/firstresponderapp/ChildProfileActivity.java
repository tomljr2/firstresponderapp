package edu.fsu.cs.mobile.firstresponderapp;

import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;

public class ChildProfileActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_child_profile);

        final TextView childName = findViewById(R.id.childName);
        final TextView childBD = findViewById(R.id.childBD);
        final TextView childGender = findViewById(R.id.childGender);
        final TextView emergencyContact = findViewById(R.id.childEmergencyContact2);
        final TextView pedContact = findViewById(R.id.childPedContact2);
        final TextView BloodType = findViewById(R.id.childBloodType2);
        final TextView meds = findViewById(R.id.childmeds2);
        final TextView allergies = findViewById(R.id.childallergies2);
        final TextView disabilities = findViewById(R.id.childdisabilities2);
        final TextView surgeries = findViewById(R.id.childsurgeries2);

        final ImageView qr = findViewById(R.id.childImage);

        final DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("Parent")
                .child(getIntent().getStringExtra("ID")).child("Children").child(getIntent()
                        .getStringExtra("childID"));

        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                childName.setText(snapshot.child("firstName").getValue().toString()
                        + " " + snapshot.child("lastName").getValue().toString());
                if(snapshot.child("birthDate").getValue() != null)
                    childBD.setText(snapshot.child("birthDate").getValue().toString());
                if(snapshot.child("gender").getValue() != null)
                    childGender.setText(snapshot.child("gender").getValue().toString());
                if(snapshot.child("emergencyContact").getValue() != null)
                    emergencyContact.setText(snapshot.child("emergencyContact").getValue().toString());
                if(snapshot.child("pediatricianContact").getValue() != null)
                    pedContact.setText(snapshot.child("pediatricianContact").getValue().toString());
                if((snapshot.child("bloodTypeLetter").getValue() != null) && (snapshot.child("bloodTypePM").getValue() != null))
                    BloodType.setText(snapshot.child("bloodTypeLetter").getValue().toString()
                            + snapshot.child("bloodTypePM").getValue().toString());
                if(snapshot.child("medications").getValue() != null)
                    meds.setText(snapshot.child("medications").getValue().toString());
                if(snapshot.child("allergies").getValue() != null)
                    allergies.setText(snapshot.child("allergies").getValue().toString());
                if(snapshot.child("cognitiveDisabilities").getValue() != null)
                    disabilities.setText(snapshot.child("cognitiveDisabilities").getValue().toString());
                if(snapshot.child("surgeries").getValue() != null)
                    surgeries.setText(snapshot.child("surgeries").getValue().toString());
                try {
                    qr.setImageBitmap(textToImage(getIntent().getStringExtra("ID"),
                            500,500));
                } catch (WriterException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {}
        });
    }

    private Bitmap textToImage(String text, int width, int height) throws WriterException, NullPointerException {
        BitMatrix bitMatrix;
        try {
            bitMatrix = new MultiFormatWriter().encode(text, BarcodeFormat.QR_CODE,
                    width, height, null);
        } catch (IllegalArgumentException Illegalargumentexception) {
            return null;
        }

        int bitMatrixWidth = bitMatrix.getWidth();
        int bitMatrixHeight = bitMatrix.getHeight();
        int[] pixels = new int[bitMatrixWidth * bitMatrixHeight];

        int colorWhite = 0xFFFFFFFF;
        int colorBlack = 0xFF000000;

        for (int y = 0; y < bitMatrixHeight; y++) {
            int offset = y * bitMatrixWidth;
            for (int x = 0; x < bitMatrixWidth; x++) {
                pixels[offset + x] = bitMatrix.get(x, y) ? colorBlack : colorWhite;
            }
        }
        Bitmap bitmap = Bitmap.createBitmap(bitMatrixWidth, bitMatrixHeight, Bitmap.Config.ARGB_4444);

        bitmap.setPixels(pixels, 0, width, 0, 0, bitMatrixWidth, bitMatrixHeight);
        return bitmap;
    }
}
