package edu.fsu.cs.mobile.firstresponderapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button parentButton = (Button) findViewById(R.id.PLogin);
        Button FRButton = (Button) findViewById(R.id.FRLogin);
        Button newAcctButton = (Button) findViewById(R.id.newAcct);

        parentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent parentLoginIntent = new Intent(getApplicationContext(), ParentLoginActivity.class);
                startActivity(parentLoginIntent);
                //Code to switch to parent login activity / fragment goes here
            }
        });

        FRButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent responderLoginIntent = new Intent(getApplicationContext(), ResponderLoginActivity.class);
                startActivity(responderLoginIntent);
                //Code to switch to first responder login activity / fragment goes here
            }
        });

        newAcctButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent newAcctIntent = new Intent(getApplicationContext(), NewAcctFragActivity.class);
                startActivity(newAcctIntent);
            }
        });

    }
}
