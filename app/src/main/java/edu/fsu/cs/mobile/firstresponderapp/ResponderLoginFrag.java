package edu.fsu.cs.mobile.firstresponderapp;

import android.app.Fragment;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class ResponderLoginFrag extends Fragment {

    public ResponderLoginFrag(){}
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_responder_login, container, false);
        final EditText username = view.findViewById(R.id.responderUser);
        final EditText password = view.findViewById(R.id.responderPass);
        Button login = view.findViewById(R.id.responderLoginButton);
        final DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("FirstResponder");

        //====================== DYNAMIC USERNAME VALIDATION ==========================

        username.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

            //Do not allow spaces in the username (more restrictions may come later)
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(hasSpace(charSequence))
                    username.setTextColor(Color.RED);
                if(!hasSpace(charSequence))
                    username.setTextColor(Color.DKGRAY);
            }

            @Override
            public void afterTextChanged(Editable editable) {}
        });

        //If the user goes out of focus on the editText that is still wrong, then notify them
        //to change it.
        username.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onFocusChange(View view, boolean b) {
                if(!username.hasFocus())
                    if(username.getText().toString().equals(""))
                        Toast.makeText(getContext(),"Please enter a username",
                                Toast.LENGTH_SHORT).show();
                if(hasSpace(username.getText()))
                    Toast.makeText(getContext(),"Please do not use spaces in " +
                            "your username",Toast.LENGTH_SHORT).show();
            }
        });

        //======================= DYNAMIC PASSWORD VALIDATION ========================

        //Currently the only restriction on passwords is to not allow it to be blank
        password.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onFocusChange(View view, boolean b) {
                if(!password.hasFocus())
                    if(password.getText().toString().equals(""))
                        Toast.makeText(getContext(),"Please enter a password",
                                Toast.LENGTH_SHORT).show();
            }
        });

        login.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                ref.addListenerForSingleValueEvent(new ValueEventListener() {
                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                            if(snapshot.child("username").getValue().toString().equals(username.getText().toString())
                                    && snapshot.child("password").getValue().toString().equals(password.getText().toString()))
                            {
                                Intent scanQR = new Intent(getActivity(), CaptureActivity.class);
                                startActivity(scanQR);
                                return;
                            }
                        }

                        Toast.makeText(getContext(), "Invalid username / password", Toast.LENGTH_SHORT).show();
                    }
                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                    }
                });

            }
        });
        return view;
    }

    boolean hasSpace(CharSequence c)
    {
        for(int i = 0; i < c.length();  i++)
            if(c.charAt(i) == ' ')
                return true;
        return false;
    }
}
