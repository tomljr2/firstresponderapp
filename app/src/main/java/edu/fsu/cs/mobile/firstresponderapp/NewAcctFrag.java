package edu.fsu.cs.mobile.firstresponderapp;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.w3c.dom.Text;

import java.util.HashMap;
import java.util.Map;

public class NewAcctFrag extends Fragment {
    public NewAcctFrag() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_new_acct, container, false);
        final EditText FNET = view.findViewById(R.id.firstNameET);
        final EditText LNET = view.findViewById(R.id.lastNameET);
        final EditText UET = view.findViewById(R.id.usernameET);
        final EditText PET = view.findViewById(R.id.passwordET);
        final Button SB = view.findViewById(R.id.submitButton);
        final Button CB = view.findViewById(R.id.clearButton);

        final DatabaseReference ref = FirebaseDatabase.getInstance().getReference();

        //------------------- DYNAMIC VALIDATION FOR FIRST NAME -------------------//

        //This is some fancy dynamic error checking that I implemented
        FNET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

            //Only allow the user to enter letters, spaces, apostrophes, and hyphens.
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(!charSequence.toString().matches("[a-zA-Z-' ]+"))
                    FNET.setTextColor(Color.RED);
                if(FNET.getText().toString().matches("[a-zA-Z-' ]+"))
                    FNET.setTextColor(Color.DKGRAY);
            }

            @Override
            public void afterTextChanged(Editable editable) {}
        });

        //If the user goes out of focus on the editText that is still wrong, then notify them
        //to change it.
        FNET.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if(!FNET.hasFocus())
                    if(!FNET.getText().toString().matches("[a-zA-Z-' ]+"))
                        if(FNET.getText().toString().equals(""))
                            Toast.makeText(getContext(),"Please enter a first name",
                                    Toast.LENGTH_SHORT).show();
                        else
                            Toast.makeText(getContext(),"Please do not use special characters",
                                    Toast.LENGTH_SHORT).show();
                else
                    FNET.setText(capitalize(FNET.getText().toString().trim()));
            }
        });

        //-------------------------------------------------------------------------//

        //------------------- DYNAMIC VALIDATION FOR LAST NAME -------------------//

        //This is the same as the first name dynamic error checking
        LNET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

            //Only allow the user to enter letters, spaces, apostrophes, and hyphens.
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(!charSequence.toString().matches("[a-zA-Z-' ]+"))
                    LNET.setTextColor(Color.RED);
                if(LNET.getText().toString().matches("[a-zA-Z-' ]+"))
                    LNET.setTextColor(Color.DKGRAY);
            }

            @Override
            public void afterTextChanged(Editable editable) {}
        });

        //If the user goes out of focus on the editText that is still wrong, then notify them
        //to change it.
        LNET.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if(!LNET.hasFocus())
                    if(!LNET.getText().toString().matches("[a-zA-Z-' ]+"))
                        if(LNET.getText().toString().equals(""))
                            Toast.makeText(getContext(),"Please enter a last name",
                                    Toast.LENGTH_SHORT).show();
                        else
                            Toast.makeText(getContext(),"Please do not use special characters",
                                    Toast.LENGTH_SHORT).show();
                    else
                        LNET.setText(capitalize(LNET.getText().toString().trim()));
            }
        });

        //-------------------------------------------------------------------------//

        //------------------- DYNAMIC VALIDATION FOR USERNAME -------------------//

        UET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

            //Do not allow spaces in the username (more restrictions may come later)
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(hasSpace(charSequence))
                    UET.setTextColor(Color.RED);
                if(!hasSpace(charSequence))
                    UET.setTextColor(Color.DKGRAY);
            }

            @Override
            public void afterTextChanged(Editable editable) {}
        });

        //If the user goes out of focus on the editText that is still wrong, then notify them
        //to change it.
        UET.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if(!UET.hasFocus())
                    if(UET.getText().toString().equals(""))
                        Toast.makeText(getContext(),"Please enter a username",
                                Toast.LENGTH_SHORT).show();
                    if(hasSpace(UET.getText()))
                        Toast.makeText(getContext(),"Please do not use spaces in " +
                                            "your username",Toast.LENGTH_SHORT).show();
            }
        });

        //-------------------------------------------------------------------------//

        //------------------- DYNAMIC VALIDATION FOR PASSWORD -------------------//

        //Currently the only restriction on passwords is to not allow it to be blank
        PET.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if(!PET.hasFocus())
                    if(PET.getText().toString().equals(""))
                        Toast.makeText(getContext(),"Please enter a password",
                                Toast.LENGTH_SHORT).show();
            }
        });

        //-------------------------------------------------------------------------//

        //------------------- SUBMIT BUTTON -------------------//

        //If there are any errors in the form, then pressing the submit button will force the user
        //to correct the errors to continue.
        SB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final boolean found = false;

                ref.child("Parent").addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {

                            if((FNET.getCurrentTextColor() == Color.RED || FNET.getText().toString().equals(""))
                                    || (LNET.getCurrentTextColor() == Color.RED || LNET.getText().toString().equals(""))
                                    || (UET.getCurrentTextColor() == Color.RED || UET.getText().toString().equals(""))
                                    || (PET.getCurrentTextColor() == Color.RED || PET.getText().toString().equals(""))
                                    || (snapshot.child("username").getValue().toString().equals(UET.getText().toString())))
                            {

                                if (snapshot.child("username").getValue().toString().equals(UET.getText().toString()))
                                {
                                    UET.setTextColor(Color.RED);
                                    Toast.makeText(getContext(), "Username taken",
                                            Toast.LENGTH_SHORT).show();
                                }
                                else
                                {
                                    Toast.makeText(getContext(), "Please correct any mistakes in the form",
                                            Toast.LENGTH_SHORT).show();
                                }
                                return;
                            }
                        }
                        //TODO - Encrypt password
                        ref.child("Parent").push().setValue(new newUser(FNET.getText().toString(),
                                LNET.getText().toString(), UET.getText().toString(),
                                PET.getText().toString()));

                        Intent intent = new Intent(getContext(), ParentAccountActivity.class);
                        intent.putExtra("username", UET.getText().toString());
                        startActivity(intent);
                    }
                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                    }
                });

            }
        });

        //-------------------------------------------------------------------------//

        //------------------- CLEAR BUTTON -------------------//

        //Very simply, just reset all values on each EditText to be empty.
        CB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FNET.setText("a"); FNET.clearFocus(); FNET.setText("");
                LNET.setText("a"); LNET.clearFocus(); LNET.setText("");
                UET.setText("a"); UET.clearFocus(); UET.setText("");
                PET.setText("a"); PET.clearFocus(); PET.setText("");
                FNET.requestFocus();    //Set focus on the first name edit text on clear
            }
        });

        //-------------------------------------------------------------------------//

        // Inflate the layout for this fragment
        return view;
    }

    //Change any string to capitalize if it is the first character or if it is preceded by a space
    String capitalize(String s)
    {
        if(s == null)
            return s;

        for(int i = 0; i < s.length(); i++)
        {
            if(i == 0)
                s = s.substring(0,1).toUpperCase() + s.substring(1);
            if(s.charAt(i) == ' ' && i+1 < s.length())
                s = s.substring(0,i+1) + s.substring(i+1,i+2).toUpperCase() + s.substring(i+2)
                        .toLowerCase();
        }

        return s;
    }

    //Check if there is a space in a charSequence
    boolean hasSpace(CharSequence c)
    {
        for(int i = 0; i < c.length();  i++)
            if(c.charAt(i) == ' ')
                return true;
        return false;
    }
}

class newUser
{
    public String firstName, lastName, username, password;

    newUser(String f, String l, String u, String p)
    {
        firstName = f;
        lastName = l;
        username = u;
        password = p;
    }
}